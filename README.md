# Trabalhos - AED2

Repositório para os trabalhos da disciplina Algoritmos e Estrutura de Dados II.

As instruções de build e uso dos programas se encontram nos respectivos
diretórios de cada trabalho.

- [Árvore Rubro-Negra](red-black-tree/)
- [Árvore B (2-3-4)](btree/)
- [Grafos](graph/)
