#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "rbt.h"

// Número que será utilizado para limitar o valor das chaves aleatórias
// inseridas no modo aleatório. O maior número a ser inserido é MAXNUM - 1
#define MAXNUM 1000

void print_help(void)
// Exibe a mensagem de help
{
  printf("Usage: rbt [OPTION]...\n"
         "A simple implementation of Red-Black Tree basic operations.\n"
         "\n"
         "This program can be run both interactive or non-interactive mode,\n"
         "where the former is the standard operation mode. The option -r\n"
         "(or --random) can be passed as argument to run the later one\n"
         "\n"
         "  -h, --help          show this helping message and exit\n"
         "  -p, --print-always  force print the tree after each operation.\n"
         "\n"
         "\e[1;70mInteractive Mode\e[0m\n"
         "The program defaults to interactive mode, where the user is\n"
         "supplied with a menu to choose an option, that can be one of\n"
         "the following:\n"
         "\n"
         "  i <number>  inserts <number> on the tree\n"
         "  r <number>  remove <number> from the tree\n"
         "  a           insert a random number\n"
         "  p           print tree\n"
         "  h           print a help message\n"
         "  q           quit\n"
         "\n"
         "\e[1;70mNon-interactive (Random Mode)\e[0m\n"
         "When executed on random mode, the program will fill the tree with\n"
         "random numbers, based on the arguments you passed. After the last\n"
         "operation, the tree will be printed.\n"
         "\n"
         "  -r, --random  <number>  enable random mode, that will insert\n"
         "                          <number> random keys into the tree.\n"
         "\n"
         "\e[1;70mExamples\e[0m\n"
         "  rbt -p -r 10  Will insert 10 random keys at the tree and print\n"
         "                it after each insertion.\n"
         "\n"
         "  rbt    This example represents interactive mode.\n"
         "  :i 10  Insert 10 into the tree.\n"
         "  :i 20  Insert 20 into the tree.\n"
         "  :i 30  Insert 30 into the tree.\n"
         "  :r 20  Remove 20 from the tree.\n"
         "  :a     Inserts a random number into the tree.\n"
         "  :p     Prints the tree.\n"
         "\n"
  );
}

void print_interactive_help()
// Exibe a mensagem de help do modo interativo
{
  printf("\e[1;70mHelp\e[0m\n"
         "  i <number>  inserts <number> on the tree\n"
         "  r <number>  remove <number> from the tree\n"
         "  a           insert a random number\n"
         "  p           print tree\n"
         "  h           print a help message\n"
         "  q           quit\n"
         "\n"
  );
}

int main(int argc, char *argv[])
{
  // Inicializa a raiz da árvore que será utilizada
  rbt_noh *root = rbt_init();

  // Se TRUE, irá forçar a exibição da árvore após cada operação
  bool print_always = 0;

  // Se TRUE, o programa rodará no modo aleatório
  bool random_mode  = 0;

  // Número de elementos a ser inserido no modo aleatório
  unsigned long treesize = 0;

  int key;

  // Utiliza o horário no momento de execução como seed para a geração
  // dos números aleatórios
  srand(time(0));

  // Tratamento de todos os argumentos.
  for(int i = 1; i < argc; i++)
    {
      // Exibe mensagem de help
      if(!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help"))
        {
      	  print_help();
          return 0;
        }

      // Habilita modo aleatório
      else if(!strcmp(argv[i], "-r") || !strcmp(argv[i], "--random"))
        {
          random_mode = 1;

          // Não foi passado nenhum argumento após -r ou --random
          if(argc <= i+1)
            {
              printf("Error: Invalid number passed to %s argument\n", argv[i]);
              return 1;
            }

          char **str_tail = NULL;
          // Converte o argumento passado após -r / --random para um inteiro
          treesize = strtoul(argv[i+1], str_tail, 10);

          // Se a conversão acima falhar (argumento não válido)
          if(!treesize)
            {
              printf("Error: Invalid number passed to %s argument\n", argv[i]);
              return 1;
            }

          // Incrementa i para que o número passado após -r / --random não seja
          // tratado por este loop
          i++;
        }

      // Habilita print da ávore após cada operação
      else if(!strcmp(argv[i], "-p") || !strcmp(argv[i], "--print"))
        print_always = 1;

      else
        {
          printf("Error: Invalid argument %s\n", argv[i]);
          return 1;
        }
    }

  // Operação no modo aleatório
  if(random_mode)
    {
      for(int i = 0; i < treesize; i++)
        {
          key = rand() % MAXNUM;
          root = rbt_insert(root, key);
          printf("Inserted %d\n", key);

          if(print_always)
            {
              rbt_print(root, root->parent, NULL, 0); 
              printf("\n");
            }
        }

      // Caso print_always seja TRUE, a ultima operação já terá exibido a
      // arvore, portanto não há necessidade de exibi-la novamente ao
      // término do programa.
      if(!print_always)
        {
          rbt_print(root, root->parent, NULL, 0); 
          printf("\n");
        }

      return 0;
    }

  // Modo interativo
  else
    {
      printf("Welcome to rbt! You are on interactive mode.\n"
             "Enter 'h' command if you need help.\n\n"
      );

      char command[20]; // Buffer que conterá o comando digitado
      char action;      // Operação inserida pelo usuário
      int  number;      // Argumento passado para a operação

      while(true)
        {
          printf("Command: ");
          fgets(command, sizeof(command), stdin);

          // Formata o comando digitado para extrair a operação e seu argumento
          sscanf(command, "%c %i", &action, &number);
          printf("\n");

          switch(action)
            {
              // Inserção
              case 'i':
                key = number;
                root = rbt_insert(root, key);
                printf("Inserted %d\n", key);

                if(print_always)
                  rbt_print(root, root->parent, NULL, 0); 

                printf("\n");
                break;

              // Remoção
              case 'r':
                key = number;
                root = rbt_remove(root, key);
                printf("Removed %d\n", key);

                if(print_always)
                  rbt_print(root, root->parent, NULL, 0); 

                printf("\n");
                break;

              // Inserção aleatória
              case 'a':
                // Limita o valor máximo do número inserido
                key = rand() % MAXNUM;

                root = rbt_insert(root, key);
                printf("Inserted %d\n", key);

                if(print_always)
                  rbt_print(root, root->parent, NULL, 0); 

                printf("\n");
                break;

              // Print da árvore
              case 'p':
                rbt_print(root, root->parent, NULL, 0); 
                printf("\n");
                break;

              // Print da mensagem de help
              case 'h':
                print_interactive_help();
                break;

              // Sai do programa
              case 'q':
                return 0;
            }
        }  
    }

  // Este retorno não deverá ser executado, mas permanece aqui para um possível
  // caso de erro não encontrado.
  return 1;
}
