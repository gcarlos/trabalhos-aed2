import networkx as nx
import pygraphviz as pgv
import csv

G = nx.Graph()

nodes_file = 'pubmed-diabetes/data/Pubmed-Diabetes.NODE.paper.tab'
with open(nodes_file, newline='') as f:
    f.readline()
    f.readline()
    reader = csv.reader(f, delimiter='\t')
    for row in reader:
        G.add_node(row[0])

edges_file = 'pubmed-diabetes/data/Pubmed-Diabetes.DIRECTED.cites.tab'
with open(edges_file, newline='') as f:
    f.readline()
    f.readline()
    reader = csv.reader(f, delimiter='\t')
    for row in reader:
        edge = (row[1].split(':')[1], row[3].split(':')[1])
        G.add_edge(*edge)

A = nx.nx_agraph.to_agraph(G)
A.layout(prog='twopi')
A.draw("graph.png")
